 function jsProcesarFormulario(idformulario, rutaaction, iddivresultado) {
    // idformulario id del formulario
    // rutaaction a donde los datos //
    // divresultado en donde se va a mostrar los resultados
   var elementosdelformulario = $('#' + idformulario + '').formSerialize(); // para
   $('#' + idformulario + '').clearForm();
    $.ajax({
        url: rutaaction,
        type: 'POST',
        async: true,
        data: elementosdelformulario,
        contentType: 'application/x-www-form-urlencoded',
        error: function () {
            $('#' + iddivresultado + '').html(data);
        },
        success: function (data) {
            $('#' + iddivresultado + '').html(data);
        }
    }); // aqui cierra el ajax

}

function jsRotaPagina(rutaachivo, iddivresultado, parametro) {

    $('#' + iddivresultado + '').empty();
    $('#' + iddivresultado + '').removeData();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: rutaachivo,
        type: 'POST',
        async: true,
        data: 'p=' + parametro,
        contentType: 'application/x-www-form-urlencoded',
        error: function () {
            $('#' + iddivresultado + '').load('/error');

        },
        success: function (data) {
            //      $('#' + iddivresultado + '').empty();
            /** para mostrar la pagina de blockeo **/
            // $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);


            //     $(document).ajaxStart(muestra()).ajaxStop(oculta());

            $('#' + iddivresultado + '').empty();
            $('#' + iddivresultado + '').html(data);
        }
    }); // aqui cierra el ajax

}


  
