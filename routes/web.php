<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::group(['as' => '/::'], function () {
    Route::get('/', 'PrincipalController@index');
    Route::get('/empresas', 'PrincipalController@empresa');
    Route::get('/listadoempresas', 'EmpresasController@index');
    Route::get('/logout', 'HomeController@logout');
    Route::post('/estados', 'EstadoController@estadosselect');
    Route::post('/ciudades', 'CiudadesController@ciudadselect');
    Route::post('/guardarempresa', 'EmpresasController@store');
});

route::group(['as' => '/dashboard'], function () {
    Route::get('/dashboard/empresas', 'EmpresasController@listadoempresas');
    Route::get('/usuarios', 'UserController@empresas');
    Route::get('/logout', 'HomeController@logout');
    Route::get('/dashboard/pais', 'PaisesController@index');
    Route::post('/dashboard/estadospais/{id}', 'EstadoController@getEstadoPais');
    Route::post('/dashboard/ciudadesestados/{id}', 'CiudadesController@getCuidadesEstado');
    Route::post('/dashboard/nuevopais', 'PaisesController@store');

    Route::post('/dashboard/empresaupdate', 'EmpresasController@update');
    Route::post('/dashboard/editar/{id}/{columna}', 'HomeController@editar');
    Route::post('/dashboard/upateempresapais', 'PaisesController@updateempresa');
    Route::post('/dashboard/updateempresaestados', 'EstadoController@updateempresaestado');
    Route::post('/dashboard/updatetipologiaempresa', 'TipologiasController@updateempresatipologia');
    Route::post('/dashboard/nuevoestado/{idpais}', 'EstadoController@create');
    Route::post('/dashboard/guardarnuevoestado/', 'EstadoController@store');
    Route::post('/dashboard/editarestado/{id}', 'EstadoController@edit');
    Route::post('/dashboard/updateestado/', 'EstadoController@update');
    Route::post('/dashboard/nuevaciudad/{idestado}', 'CiudadesController@create');
    Route::post('/dashboard/guardarnuevaciudad/', 'CiudadesController@store');
    Route::post('/dashboard/editarciudad/{idciudad}', 'CiudadesController@edit');
    Route::post('/dashboard/guardarupdateciudad/', 'CiudadesController@update');
    Route::post('/dasborad/buscarempresa/', 'EmpresasController@Buscar');


});

Auth::routes();
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/home', 'HomeController@index')->name('dashboard');
