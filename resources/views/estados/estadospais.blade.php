<script type="text/javascript">
    $(document).ready(function () {
        $('#estados').change(function () {
            if ($(this).val() === 'Selecione') {
                $(this).addClass('alert-danger');
            }
            else {
                $(this).removeClass();
                $(this).addClass('form-control');
                jsRotaPagina('{{ url('/ciudades')}}', 'divCiudades', $(this).val());
            }
        });
    });
</script>
<div class="panel panel-default">
    <div class="panel-body">
         <form action="{{url('/dashboard/updateempresaestados') }}" method="post" role="form">
            {{csrf_field() }}
            <input type="hidden" value="{{$id}}" name="id" id="id">
            <div class="form-group">
                <label class="" for="">Estados</label>
                <select name="estados" id="estados" class="form-control alert-danger">
                    <option value="Selecione">Seleccione el Estado</option>
                    @foreach($estados as $estado)
                        <option value="{{$estado->id_estado }}">{{$estado->estado }} </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
            <label class="" for="">Ciudades</label>
                <div id="divCiudades"style="height:100px" ></div>
            </div>
            <div class="panel-footer pull-right">
                <button type="submit" class="btn btn-default">Guadar Cambios</button>
            </div>
        </form>
    </div>
</div>