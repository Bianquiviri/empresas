<ul class="list-group ">
    @foreach($estados as $estado)
        <li class="list-group-item">{{ $estado->estado }}

            <a class="btn btn-xs btn-default  pull-right"
               onclick="jsRotaPagina('{{url('dashboard/editarestado',$estado->id_estado)}}','divModal','NoAplica')" data-toggle="modal" href="#modal-id" >Editar</a>



            <a class="btn btn-xs btn-default  pull-right"
               onclick="jsRotaPagina('{{url('dashboard/nuevaciudad',$estado->id_estado)}}','divModal','NoAplica')" data-toggle="modal" href="#modal-id" >Nueva</a>
            <a class="btn btn-xs  btn-default pull-right"
               onclick="jsRotaPagina('{{url('dashboard/ciudadesestados',$estado->id_estado)  }}','divCiudad','NoAplica')">Ciudades</a>
        </li>
    @endforeach
</ul>
