<div class="text-center">
    <form action="{{ url('/dashboard/updateestado') }}" method="post" class="form-inline" role="form">
        {{ csrf_field() }}
        <div class="form-group">
            <input type="hidden" class="form-control" name="idestado" id="idestado" value="{{ $id }}">
            <input type="text" class="form-control" name="estado" id="estado" value="{{$estado->estado  }} " required>
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>

    </form>
</div>
