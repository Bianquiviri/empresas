@extends('layouts.template')
@section('content')
    @if (Session::has('message'))
        <div id="mensaje">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong></strong>{{ Session::get('message') }}
            </div>
               </div>
   @endif
    <div class="box ">
        <div class="box-header">
            <i class="fa fa-building"></i>
            <h3 class="box-title">Empresas </h3>
            <div class="box-tools">
                <span class="badge">{{$empresas->total()  }}</span>
            </div>
        </div>
        <div class="box-body">
            <div class="alert alert-info">
                 <strong>Informaci&oacute;n !!</strong>
                Para editar solo tienes que iniciar sessi&oacute;n
                    <a href="{{url('login')}}"  class="btn btn-success">
                        Ingresar
                    </a>
            </div>
            <div class="row">
                    @foreach($empresas as $empresa)
                        <div   class="col-md-4 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4><i class="fa fa-fw fa-check"></i> {{ $empresa->nombre_empresa }}</h4>
                            </div>
                            <div class="panel-body">
                                <div class="box box-solid">
                                    <div class="box-header">
                                        <i class="fa fa-text-width"></i>
                                        <h3 class="box-title"> Informaci&oacute;n Agregadas</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <dl class="dl-horizontal">
                                            <dt>Nombre</dt>
                                            <dd>{{ $empresa->nombre_empresa }}</dd>
                                            <dt>Pais</dt>
                                            <dd> {{ App\Http\Controllers\PaisesController::getNombrePais($empresa->id_empresa)}}</dd>
                                            <dt>Estado</dt>
                                            <dd>{{App\Http\Controllers\EstadoController::getEstado($empresa->id_empresa)}}  </dd>
                                            <dt>Ciudad</dt>
                                            <dd> {{App\Http\Controllers\CiudadesController::getCuidad($empresa->id_empresa)}} </dd>
                                            <dt>Descripci&oacute;n</dt>
                                            <dd><p class="text text-justify pre-scrollable" > {{$empresa->descripcion_empresa  }} </dd></p>
                                            <dt>Tipo</dt>
                                            <dd>{{App\Http\Controllers\TipologiasController::getTipologia($empresa->id_empresa)  }} </dd></p>

                                        </dl>
                                    </div>
                                </div>


                            </div>
                        </div>
                </div>
                @endforeach
                </div>
            <div class="box-footer pull-right">
                    {{$empresas->links()}}
                </div>
            </div>
@endsection