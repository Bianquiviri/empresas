<footer>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <h4>Empresas</h4>
            <p class="text text-justify">
                Nullam euismod lacus sed ipsum scelerisque, vulputate mattis ante viverra.
                Etiam porttitor sapien id ante condimentum tristique.
                Morbi nec leo vitae velit pretium vestibulum.
                Duis sit amet lacus facilisis, egestas magna quis, tempus ex.
            </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <h4><b>Servicios</b></h4>
            <p class="text text-justify">
                Nullam euismod lacus sed ipsum scelerisque, vulputate mattis ante viverra.
                Etiam porttitor sapien id ante condimentum tristique.
                Morbi nec leo vitae velit pretium vestibulum.
                Duis sit amet lacus facilisis, egestas magna quis, tempus ex.
            </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <h4><b>Valora un Servicio</b></h4>
            <p class="text-justify "> Nullam euismod lacus sed ipsum scelerisque, vulputate mattis ante
                                      viverra.
                                      Etiam porttitor sapien id ante condimentum tristique.
                                      Morbi nec leo vitae velit pretium vestibulum.
                                      Duis sit amet lacus facili</p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <h4><b>Contacto</b></h4>
            <p class="text text-justify">
                Nullam euismod lacus sed ipsum scelerisque, vulputate mattis ante viverra.
                Etiam porttitor sapien id ante condimentum tristique.
                Morbi nec leo vitae velit pretium vestibulum.
                Duis sit amet lacus facilisis, egestas magna quis, tempus ex.
            </p>
        </div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-offset-1">

                <hr>
                <h4><b>Opiniones sobre servicios sanitarios , funerarios y mucho m&aacute;s </b></h4>
                <hr>
                @2017 Todos los servicios reveservados <a href="{{ url('codiciones#codiciones') }}">
                    Codiciones de Uso </a>,
                <a href="{{ url('codiciones#politicas') }}"> Politicas de Privacidad </a> ,
                <a href="{{ url('codiciones') }}"> Politicas de Cookies </a></br>
                <p class="text text-justify text-center">
                    Nulla vel gravida nulla. Vivamus malesuada tincidunt lorem, nec bibendum mi tristique vitae.
                    Vivamus
                    vulputate ex eleifend leo porta auctor. Maecenas a molestie est, in viverra erat. Sed quis
                    imperdiet
                    libero, in facilisis dolor.
                </p>
                <span class="center-block"> VERITUS@2017</span>

            </div>


    </div>

</footer>
