<div class="text-center">
    <form action="{{ url('/dashboard/guardarupdateciudad') }}" method="post" class="form-inline" role="form">
        {{ csrf_field() }}
        <div class="form-group">
            <input type="hidden" class="form-control" name="idciudad"  value="{{ $id }}">
            <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Nueva Ciudad" value="{{$ciudad->ciudad }}"  required>
        </div>
        <button type="submit" class="btn btn-primary">Agregar</button>

    </form>
</div>
