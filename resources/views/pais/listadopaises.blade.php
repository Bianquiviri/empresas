@extends('dashboard.template')
@section('content')
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Paises Agregados </h3>
        </div>
        <div class="panel-body">
            <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form action="{{ url('/dashboard/nuevopais') }}" method="post" class="form-inline" role="form">
                {{ csrf_field() }}
                    <div class="form-group">
                         <input type="text" class="form-control" name="pais" id="pais" placeholder="Nuevo Pais" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Agregar</button>
                <br>
                </form>
            </div>
                <hr>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <table class="table table-responsive table-bordered">
                        <thead>
                        <tr>
                            <th>
                              Pais
                            </th>
                            <th>
                                Estados
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($paises as $pais)
                                <td>{{$pais->nombre_pais }} </td>
                                <td>
                                    <button class="btn btn-sm btn-default" onclick="jsRotaPagina('{{url('dashboard/estadospais',$pais->id_pais)  }}','divEstados','NoAplica')" >Ver Estados</button>
                                    <button class="btn btn-sm btn-default" onclick="jsRotaPagina('{{url('dashboard/nuevoestado',$pais->id_pais)  }}','divModal','NoAplica') " data-toggle="modal" href="#modal-id" >Nuevo </button>
                                </td>
                        </tr>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div id="divEstados">Seleccione el Pais </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div id="divCiudad"> Seleccione al cuidad</div>
                </div>
        </div>
    </div>
    </div>
        <div class="modal fade" id="modal-id">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Agregar Items</h4>
                </div>
                <div class="modal-body">
                 <div id="divModal"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection