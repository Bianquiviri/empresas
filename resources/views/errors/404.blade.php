<!-- Latest compiled and minified CSS & JS -->
<link rel="stylesheet" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<html lang="es" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="author" content="AulaSoft de estudios Tecnologicos  Informaticos">
        <link rel="icon" href="{{asset('imagenes/logo.png') }}">
        <meta charset="UTF-8">
        <title>{{ $page_title or "Sistema de Registros de Empresas" }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
              name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="{{ asset("/bootstrap-3.3.7/css/bootstrap.css") }}" rel="stylesheet"
              type="text/css"/>
        <!-- tema bootstrap -->
        <!-- Para mejorar los espacios  -->
    </head>
    <body style="padding-top: 100px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <div class="error-template">
                        <img src="{{asset('imagenes/logo.png')  }}">

                        <h2 class="text text-danger"> Error 404 pagina no encontrada  </h2>
                        <div class="error-details">
                            Lo siento su petici&oacute;n no se ha podido procesar
                        </div>
                        <div class="error-actions">
                            <br>
                            <a href="{{url('/')  }}" class="btn btn-primary btn-lg"><span
                                        class="glyphicon glyphicon-home"></span>
                                Volver</a><a href="{{url('contacto')}}"
                                             class="btn btn-default btn-lg"><span
                                        class="glyphicon glyphicon-envelope"></span> Soporte </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </body>
    </html>
