<form action="{{url('dashboard/updatetipologiaempresa')  }}" method="post" class="form-inline" role="form">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="id" value="{{$id}}">
    <div class="form-group">
        <label class="" for="">Tipologias </label>
        <select id="tipos" name="tipos" class="form-control">
            @foreach($tipologias as $tipo)
                <option value="{{$tipo->id_tipologia}}"> {{$tipo->nombre_tipologiaempresa }} </option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>