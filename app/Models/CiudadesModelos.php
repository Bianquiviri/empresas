<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CiudadesModelos extends Model
{
    //tabla cuidades
    public $primaryKey = 'id_ciudad';
    protected $table = 'ciudades';
}
