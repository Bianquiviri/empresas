<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipologiaModelo extends Model
{
    // Para manipular la tabla tipoligias
    public $primaryKey = 'id_tipologia';
    protected $table = 'tipologias';
}
