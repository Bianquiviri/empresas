<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaisesModelo extends Model
{
    //Tabla Paises
    public $primaryKey = 'id_pais';
    protected $table = 'paises';

}
