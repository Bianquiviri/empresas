<?php

namespace App\Http\Controllers;
use App\Models\EmpresasDetalleModelo;
use App\Models\PaisesModelo;
use Illuminate\Http\Request;

class PaisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Para mostra el listado de los paises
        $paises= PaisesModelo::orderBy('nombre_pais','asc')->get();
        $home = new HomeController();
        $usuario = $home->getUsuario();
        $page_title ='Paises';
        return \view('pais.listadopaises',\compact(['paises','usuario','page_title']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // parar guardar  un neuvo pais
        $NuevoPais = new PaisesModelo();
        $NuevoPais->nombre_pais = $request->pais;
        $NuevoPais->save();
        return \redirect('/dashboard/pais');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //para mostrar los paises;
        // para saber que paises selecciono
        // el id el de la empresa
        $empresas = EmpresasDetalleModelo::join('empresas', 'empresa_detalle.id_empresa', '=', 'empresas.id_empresa')
            ->join('paises', 'empresa_detalle.id_pais', '=', 'paises.id_pais')
            ->where('empresa_detalle.id_empresa', $id)
            ->select('paises.*')
            ->first();
        //para el listado de los pais
            if(!isset($empresas)){
                $paiseleccionado = 'Pais no encontrado';
            }else{
                $empresas->nombre_pais;
            }
        $paises = PaisesModelo::orderBy('nombre_pais', 'asc')->get();

        return \view('pais.editarempresapais', \compact(['paises', 'paiseleccionado', 'id']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateempresa(Request $request)
    {
        // Para actualiar el pais y eliminar los el estado y la cuidad
        $id = $request->id;
        $idNuevoPais = $request->paises;
        $DetalleEmpresa = EmpresasDetalleModelo::where('id_empresa', $id)->first();
        $UpdateDetalleEmpresa = EmpresasDetalleModelo::find($DetalleEmpresa->id_detalleempresa);
        $UpdateDetalleEmpresa->id_pais = $idNuevoPais;
        $UpdateDetalleEmpresa->id_estado = "0";
        $UpdateDetalleEmpresa->id_ciudad = "0";
        $UpdateDetalleEmpresa->save();
        return \redirect('/dashboard/empresas')->with('message', 'Se ha modificado co exito el Pais');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getPaises()
    {
        // devuelve los paises agregados
        $Paises = PaisesModelo::orderBy('nombre_pais', 'asc')->paginate(10);
        return $Paises;
    }
    static function getNombrePais($id_empresa){
        $Pais = EmpresasDetalleModelo::join('paises','empresa_detalle.id_pais','=','paises.id_pais')
                                      ->where('empresa_detalle.id_empresa',$id_empresa)
                                      ->select('paises.nombre_pais')->first ();

        if(!isset($Pais)){
          return   $Pais = 'Pais no encontrado';
        }else{
            return $Pais->nombre_pais;
        }


    }

}
