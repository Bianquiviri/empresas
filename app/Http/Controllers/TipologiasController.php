<?php

namespace App\Http\Controllers;

use App\Models\EmpresasDetalleModelo;
use App\Models\TipologiaModelo;
use Illuminate\Http\Request;

class TipologiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // para mostra la tipologia agregadas
        $tipologias = TipologiaModelo::orderBy('nombre_tipologiaempresa','asc')->get();
        return \view('tipologias.tipologias', \compact('tipologias','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateempresatipologia(Request $request){
        // para actualizar la tipologia
        $id = $request->id;
        $DetalleEmpresa = EmpresasDetalleModelo::find($id);
        $DetalleEmpresa->id_tipologia = $request->tipos;
        $DetalleEmpresa->save();
        return \redirect('/dashboard/empresas')->with('message', 'Se ha modificado con exito el esatado y el pais ');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    static function getTipologia($id_empresa){
        // para devolver el estado
        $Tipologia = EmpresasDetalleModelo::join('tipologias','empresa_detalle.id_tipologia','=','tipologias.id_tipologia')
            ->where('empresa_detalle.id_empresa',$id_empresa)
            ->select('tipologias.*')->first ();
        if(!isset($Tipologia)){
            return   $tipo = 'Tipologia no encontrada';
        }else{
            return $Tipologia->nombre_tipologiaempresa;
        }
        return $Ciudades->ciudad;
    }
}
