<?php

namespace App\Http\Controllers;

use App\Models\TipologiaModelo;

class PrincipalController extends Controller
{
    // Controlador para manejar la vista publica
    public function index()
    {
        /**
         * Para cargar la pagina principal
         */
     return  $this->empresa();
    }
    public function empresa(){
        // Para mostrar el registro de la empresa
        $page_title = "Empresa";
        $active = 'empresa';
        $breadcrumbs = 'empresa';
        $pais = new PaisesController();
        $paises = $pais->getPaises();

        // Topoligas
        $tipologias = TipologiaModelo::orderBy('nombre_tipologiaempresa')->get();
        return \view('empresas.empresa', \compact(['page_title','breadcrumbs','active','paises','tipologias']));

    }


}
